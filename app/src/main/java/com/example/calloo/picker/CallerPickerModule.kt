package com.example.calloo.picker

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val callerPickerModules = module {
    viewModel { CallerPickerViewModel() }
}